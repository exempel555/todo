# -*- coding: utf-8 -*-

import os
basedir = os.path.abspath(os.path.dirname(__file__))

DEBUG = False

SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'tasks.db')

CSRF_ENABLED = True
SECRET_KEY = 'development key'

try:
	from local_config import *
except ImportError:
	# No local config
	pass